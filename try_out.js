const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch();
  const page = await browser.newPage();

  // Open the todo app
  await page.goto('https://youtube.com'); // Change URL to your todo app URL

  // Add a todo item
  await page.fill('input[type="text"]', 'Buy groceries');
  await page.keyboard.press('Enter');

  // Check if the todo item is added
  const todoItem = await page.waitForSelector('.todo-item');
  if (await todoItem.innerText() !== 'Buy groceries') {
    console.error('Failed to add todo item');
    await browser.close();
    return;
  }

  // Mark the todo item as completed
  await page.click('.todo-item input[type="checkbox"]');
  await page.waitForSelector('.todo-item.completed');

  // Delete the todo item
  await page.click('.todo-item .delete-btn');
  await page.waitForSelector('.todo-list-empty');

  // Verify that the todo item is deleted
  const todoListEmptyMessage = await page.$('.todo-list-empty');
  if (!todoListEmptyMessage) {
    console.error('Failed to delete todo item');
    await browser.close();
    return;
  }

  console.log('Todo app test passed successfully');

  // Close the browser
  await browser.close();
})();